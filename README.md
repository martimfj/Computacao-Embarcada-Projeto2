# Projeto 2 - Reserva-e
Martim Ferreira José e Bruna Kimura

## Introdução

Um embarcado com a funcionalidade de checar se uma sala está vazia ou não a partir de um sensor de presença, além disso os dados são enviados para um servidor na núvem e os dados são disponibilizados na web. Conta com o auxilio de LED's para informar se uma sala está ocupado ou não, e caso esteja vazio se já possui reserva ou não.

## Problema

Analisando alguns problemas encontrados dentro da faculdade, notou-se a dificuldade em encontrar salas de estudos disponíveis na biblioteca. Para checar se uma sala é utilizável ou não, é necessário descer até o térreo onde se encontra as salas de estudo no fundo da biblioteca e ficar girando entre as dezenas de salas até encontrar alguma disponível. Essa falta de otimização das salas, e o gasto de tempo e energia a procura de uma sala foi o que nos impulsionou para fazer um sistema de procura e reserva de salas.

## Aplicação

A aplicação consistia em utilizar uma placa ATMEL SAME-70. Entre as conexões com a placa, foi utilizado o sensor de presença, para assim, saber se há alguém na sala ou não. Os dados coletados são enviados para um servidor na Amazon via Wi-Fi, utilizando um módulo Wi-Fi conectado na placa. Os dados são exibidos em uma página web, por meio de uma informação do status da sala (Cheia/Vazia). Caso a sala esteja cheia, não existe ação disponível na página, caso contrário é possível ver se a sala já foi reservado ou está disponível. Caso a sala esteja disponível é possível fazer a reserva. Para tanto, basta clicar no botão de reserva e assim gerará um PIN na página. Para concluir a reserva é necessário colocar este PIN no teclado conectado na saída EXT2 da placa, dentro de 5 minutos. Caso o PIN inserido esteja correto a reserva é concluida, caso demore mais de 5 minutos para inserir o PIN a sala é cancelada. Outra forma de fazer o cancelamento da sala é ir diretamente no site e clicar no botão "cancelar".

## Placa de desenvolvimento junto com o sensor, LED's e módulo Wi-Fi
![alt text](embarcado.jpeg)

## Uma das interfaces da página WEB
![alt text](site.png)

## Viabilidade

Pensando na viabilidade do projto, foram feitos os seguintes cálculos:
    - Sensor de presença - R$ 10,90
    - Teclado matricial 4x3 - R$ 10,90
    - 2 LEDs, 2 resistores ~ R$ 2,00
    - Placa de desenvolvimento Atmel SAM E70 - 150 reais
    - Módulo Wi-Fi Atmel Winc 1500 - 92 reais
Total: R$ 295,80

Pode-se pensar que este custo reduziria consideravelmente se fosse utilizada uma placa mais compacta, e pensando que uma placa pudesse controlar mais de uma sala. Também, pode-se pensar que o custo dos equipamentos reduziria se fossem comprados em grande escala.


## Diagrama de blocos da aplicação
![alt text](Diagrama de Blocos - Embarcados Final - Page 1 (2).png)

