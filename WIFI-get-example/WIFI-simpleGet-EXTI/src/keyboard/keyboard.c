/*
    Código para Keyboard de membrana 12 teclas feito por Frederico Curti e Raphael Costa
    Código original em: https://gitlab.com/raphacosta27/Computacao-Embarcada/blob/master/SAME70_TECLADO/SAME70_TECLADO/src/main.c
	Código tranformado em biblioteca por Bruna e Martim.
*/
#include "keyboard.h"

char listen_keyboard(void) {
	pio_clear(KEYBOARD_COLUMN_1_PIO, KEYBOARD_COLUMN_1_MASK); //Habilita Coluna 1
	
	//Varre linhas
	if(!pio_get(KEYBOARD_LINE_1_PIO, PIO_INPUT, KEYBOARD_LINE_1_MASK)) {
		return '1';
		//Coluna 1 Linha 1
	}
	if(!pio_get(KEYBOARD_LINE_2_PIO, PIO_INPUT, KEYBOARD_LINE_2_MASK)) {
		return '4';
		//Coluna 1 Linha 2
	}
	if(!pio_get(KEYBOARD_LINE_3_PIO, PIO_INPUT, KEYBOARD_LINE_3_MASK)) {
		return '7';
		//Coluna 1 Linha 3
	}
	if(!pio_get(KEYBOARD_LINE_4_PIO, PIO_INPUT, KEYBOARD_LINE_4_MASK)) {
		return '*';
		//Coluna 1 Linha 4
	}
	pio_set(KEYBOARD_COLUMN_1_PIO, KEYBOARD_COLUMN_1_MASK); //Desabilita Coluna 1
	
	
	pio_clear(KEYBOARD_COLUMN_2_PIO, KEYBOARD_COLUMN_2_MASK); //Habilita Coluna 2
	
	//Varre linhas
	if(!pio_get(KEYBOARD_LINE_1_PIO, PIO_INPUT, KEYBOARD_LINE_1_MASK)) {
		return '2';
		//Coluna 2 Linha 1
	}
	if(!pio_get(KEYBOARD_LINE_2_PIO, PIO_INPUT, KEYBOARD_LINE_2_MASK)) {
		return '5';
		//Coluna 2 Linha 2
	}
	if(!pio_get(KEYBOARD_LINE_3_PIO, PIO_INPUT, KEYBOARD_LINE_3_MASK)) {
		return '8';
		//Coluna 2 Linha 3
	}
	if(!pio_get(KEYBOARD_LINE_4_PIO, PIO_INPUT, KEYBOARD_LINE_4_MASK)) {
		return '0';
		//Coluna 2 Linha 4
	}
	pio_set(KEYBOARD_COLUMN_2_PIO, KEYBOARD_COLUMN_2_MASK); //Desabilita Coluna 2
	
	
	pio_clear(KEYBOARD_COLUMN_3_PIO, KEYBOARD_COLUMN_3_MASK); //Habilita Coluna 3
	
	//Varre linhas
	if(!pio_get(KEYBOARD_LINE_1_PIO, PIO_INPUT, KEYBOARD_LINE_1_MASK)) {
		return '3';
		//Coluna 3 Linha 1
	}
	if(!pio_get(KEYBOARD_LINE_2_PIO, PIO_INPUT, KEYBOARD_LINE_2_MASK)) {
		return '6';
		//Coluna 3 Linha 2
	}
	if(!pio_get(KEYBOARD_LINE_3_PIO, PIO_INPUT, KEYBOARD_LINE_3_MASK)) {
		return '9';
		//Coluna 3 Linha 3
	}
	if(!pio_get(KEYBOARD_LINE_4_PIO, PIO_INPUT, KEYBOARD_LINE_4_MASK)) {
		return '#';
		//Coluna 3 Linha 4
	}
	pio_set(KEYBOARD_COLUMN_3_PIO, KEYBOARD_COLUMN_3_MASK);
	return 'x';
}

void configure_keyboard(void){

    pio_configure(KEYBOARD_LINE_1_PIO, PIO_INPUT, KEYBOARD_LINE_1_MASK, PIO_PULLUP);
    pmc_enable_periph_clk(KEYBOARD_LINE_1_PIO_ID);

	pio_configure(KEYBOARD_LINE_2_PIO, PIO_INPUT, KEYBOARD_LINE_2_MASK, PIO_PULLUP);
    pmc_enable_periph_clk(KEYBOARD_LINE_2_PIO_ID);

	pio_configure(KEYBOARD_LINE_3_PIO, PIO_INPUT, KEYBOARD_LINE_3_MASK, PIO_PULLUP);
    pmc_enable_periph_clk(KEYBOARD_LINE_2_PIO_ID);

	pio_configure(KEYBOARD_LINE_4_PIO, PIO_INPUT, KEYBOARD_LINE_4_MASK, PIO_PULLUP);
    pmc_enable_periph_clk(KEYBOARD_LINE_3_PIO_ID);

	pio_configure(KEYBOARD_COLUMN_1_PIO, PIO_OUTPUT_1, KEYBOARD_COLUMN_1_MASK, PIO_DEFAULT);
    pmc_enable_periph_clk(KEYBOARD_COLUMN_1_PIO_ID);

	pio_configure(KEYBOARD_COLUMN_2_PIO, PIO_OUTPUT_1, KEYBOARD_COLUMN_2_MASK, PIO_DEFAULT);
    pmc_enable_periph_clk(KEYBOARD_COLUMN_2_PIO_ID);

	pio_configure(KEYBOARD_COLUMN_3_PIO, PIO_OUTPUT_1, KEYBOARD_COLUMN_3_MASK, PIO_DEFAULT);
    pmc_enable_periph_clk(KEYBOARD_COLUMN_3_PIO_ID);
}

