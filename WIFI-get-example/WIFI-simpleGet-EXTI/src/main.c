#include "asf.h"
#include "main.h"
#include <string.h>
#include "bsp/include/nm_bsp.h"
#include "driver/include/m2m_wifi.h"
#include "socket/include/socket.h"
#include "json/jsmn.h"
#include "keyboard/keyboard.h"
#include <stdlib.h>

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- WINC1500 weather client example --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL
	
/**
 *  Informacoes para o RTC
 *  poderia ser extraida do __DATE__ e __TIME__
 *  ou ser atualizado pelo PC.
 */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0

/* LEDS */
#define AMARELO_PIO PIOC
#define AMARELO_PIO_ID ID_PIOC
#define AMARELO_PIN 13
#define AMARELO_PIN_MASK (1<<AMARELO_PIN)

#define VERMELHO_PIO PIOD
#define VERMELHO_PIO_ID ID_PIOD
#define VERMELHO_PIN 11
#define VERMELHO_PIN_MASK (1<<VERMELHO_PIN)

//LED placa
#define LEDPLACA_PIO PIOC
#define LEDPLACA_PIO_ID ID_PIOC
#define LEDPLACA_PIN 8
#define LEDPLACA_PIN_MASK (1<<LEDPLACA_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIN 11
#define BUT_PIN_MASK (1<<BUT_PIN)

#define SENSOR_PIO PIOD
#define SENSOR_PIO_ID ID_PIOD
#define SENSOR_PIO_PIN 26
#define SENSOR_PIO_PIN_MASK (1<<SENSOR_PIO_PIN)

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/
void BUTs_init(void);
void LEDs_init(void);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

/** IP address of host. */
uint32_t gu32HostIp = 0;

/** TCP client socket handlers. */
static SOCKET tcp_client_socket = -1;

/** Receive buffer definition. */
static uint8_t gau8ReceivedBuffer[MAIN_WIFI_M2M_BUFFER_SIZE] = {0};

/** Wi-Fi status variable. */
static bool gbConnectedWifi = false;

/** FLAGS */
volatile uint8_t flag_sala_status = 0; //0: Vazia; 1: Cheia
volatile uint8_t flag_reservada = 0;
volatile uint8_t flag_alarm_enabled = 0;
volatile uint32_t flag_sensor = 0;

/* Password Vector */
char input[10];
char password_array[10];

/** Keyboard variable */
char key = 'x';

/** Get host IP status variable. */
/** Wi-Fi connection state */
static uint8_t wifi_connected;

/** Instance of HTTP client module. */
static bool gbHostIpByName = false;

/** TCP Connection status variable. */
static bool gbTcpConnection = false;

/** Server host name. */
static char server_host_name[] = MAIN_SERVER_NAME;

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

void RTC_Handler(void){
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	/* Time or date alarm */
	if((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		uint32_t h, m, s;
		rtc_get_time(RTC, &h, &m, &s);
		
		/*Tratamento da interrupção */
		printf("Tempo esgotado, reserva cancelada. Sala disponível.\n");

		flag_alarm_enabled = 0;
		//O cancelamento da reserva deve ser enviado ao servidor.


	}
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}


/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
	strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 0;
	}
	return -1;
}

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);
}

void enableAlarm(void){
	uint32_t h, m, s;
	rtc_get_time(RTC, &h, &m, &s);
	if (m + 5 >= 60){
		rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
		rtc_set_time_alarm(RTC, 1, h + 1, 1, m + 1 - 60, 1, s);
		printf("Alarme set to: %d:%d\n", h+1, m+1);
	}
	else {
		rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
		rtc_set_time_alarm(RTC, 1, h, 1, m + 1, 1, s);
		printf("Alarme set to: %d:%d\n", h, m+1);
	}
}

void getTime(void){
	uint32_t h, m, s;
	rtc_get_time(RTC, &h, &m, &s);
	printf("Hora atual: %d:%d\n", h, m);
}

/* 
 * Check whether "cp" is a valid ascii representation
 * of an Internet address and convert to a binary address.
 * Returns 1 if the address is valid, 0 if not.
 * This replaces inet_addr, the return value from which
 * cannot distinguish between failure and a local broadcast address.
 */
 /* http://www.cs.cmu.edu/afs/cs/academic/class/15213-f00/unpv12e/libfree/inet_aton.c */
int inet_aton(const char *cp, in_addr *ap){
  int dots = 0;
  register u_long acc = 0, addr = 0;

  do {
	  register char cc = *cp;

	  switch (cc) {
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
	        acc = acc * 10 + (cc - '0');
	        break;

	    case '.':
	        if (++dots > 3) {
		    return 0;
	        }
	        /* Fall through */

	    case '\0':
	        if (acc > 255) {
		    return 0;
	        }
	        addr = addr << 8 | acc;
	        acc = 0;
	        break;

	    default:
	        return 0;
    }
  } while (*cp++) ;

  /* Normalize the address */
  if (dots < 3) {
	  addr <<= 8 * (3 - dots) ;
  }

  /* Store it if requested */
  if (ap) {
	  ap->s_addr = _htonl(addr);
  }

  return 1;    
}


/**
 * \brief Callback function of IP address.
 *
 * \param[in] hostName Domain name.
 * \param[in] hostIp Server IP.
 *
 * \return None.
 */
static void resolve_cb(uint8_t *hostName, uint32_t hostIp)
{
	gu32HostIp = hostIp;
	gbHostIpByName = true;
	printf("resolve_cb: %s IP address is %d.%d.%d.%d\r\n\r\n", hostName,
			(int)IPV4_BYTE(hostIp, 0), (int)IPV4_BYTE(hostIp, 1),
			(int)IPV4_BYTE(hostIp, 2), (int)IPV4_BYTE(hostIp, 3));
}

/**
 * \brief Callback function of TCP client socket.
 *
 * \param[in] sock socket handler.
 * \param[in] u8Msg Type of Socket notification
 * \param[in] pvMsg A structure contains notification informations.
 *
 * \return None.
 */
static void socket_cb(SOCKET sock, uint8_t u8Msg, void *pvMsg){
	/* Check for socket event on TCP socket. */
	if (sock == tcp_client_socket) {
    
	switch (u8Msg) {
		case SOCKET_MSG_CONNECT:{
			//printf("socket_msg_connect\n");
			if(gbTcpConnection){
				memset(gau8ReceivedBuffer, 0, sizeof(gau8ReceivedBuffer));
				if(flag_sala_status == 1){ //Sala cheia
					sprintf((char *)gau8ReceivedBuffer, "%s%s%s", MAIN_PREFIX_BUFFER, (char *)MAIN_VARIAVEL_CHEIA, MAIN_POST_BUFFER);
				}
				if(flag_sala_status == 0){ //Sala vazia
					sprintf((char *)gau8ReceivedBuffer, "%s%s%s", MAIN_PREFIX_BUFFER, (char *)MAIN_VARIAVEL_VAZIA, MAIN_POST_BUFFER);
				}
				
				tstrSocketConnectMsg *pstrConnect = (tstrSocketConnectMsg *)pvMsg;
				if (pstrConnect && pstrConnect->s8Error >= SOCK_ERR_NO_ERROR) {
					//printf("send \n");
					send(tcp_client_socket, gau8ReceivedBuffer, strlen((char *)gau8ReceivedBuffer), 0);

					memset(gau8ReceivedBuffer, 0, MAIN_WIFI_M2M_BUFFER_SIZE);
					recv(tcp_client_socket, &gau8ReceivedBuffer[0], MAIN_WIFI_M2M_BUFFER_SIZE, 0);
				}
				else {
					//printf("socket_cb: connect error!\r\n");
					gbTcpConnection = false;
					close(tcp_client_socket);
					tcp_client_socket = -1;
				}
			}
		}
		break;

		case SOCKET_MSG_RECV:{
			char *pcIndxPtr;
			char *pcEndPtr;

			tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
			if (pstrRecv && pstrRecv->s16BufferSize > 0) {
        		//printf("\n %s \n\n",pstrRecv->pu8Buffer);
				char *pmsg;	
				char *res2;		
				pmsg = strstr(pstrRecv->pu8Buffer, "{\n");
					
				if(pmsg){
					//printf(pmsg);
				
					jsmn_parser p;
					jsmntok_t t[128]; /* We expect no more than 128 tokens */
					int i;
					int r;
					jsmn_init(&p);
					r = jsmn_parse(&p, pmsg, strlen(pmsg), t, sizeof(t)/sizeof(t[0]));
					if (r < 0) {
						printf("Failed to parse JSON: %d\n", r);
						//return 1;
					}
				
					/* Assume the top-level element is an object */
					if (r < 1 || t[0].type != JSMN_OBJECT) {
						printf("Object expected\n");
						//return 1;
					}
					/* Loop over all keys of the root object */
					for (i = 1; i < r; i++) {
						if (jsoneq(pmsg, &t[i], "reserva") == 0) {
							res2 = strndup(pmsg + t[i+1].start, t[i+1].end-t[i+1].start);
							//printf("- reserva: %s\n", res2);
							if(strcmp("nao",res2) == 0){
								pio_set(AMARELO_PIO, AMARELO_PIN_MASK);
								flag_reservada = 0;
								//Sala not reservada, APAGAR LED
								rtc_disable_interrupt(RTC,  RTC_IER_ALREN);
							}
							if(strcmp("sim",res2) == 0){
								pio_clear(AMARELO_PIO, AMARELO_PIN_MASK);
								flag_reservada = 1;
								//Sala reservada, ACENDER LED
							}
							i++;
						}
						else if (jsoneq(pmsg, &t[i], "status") == 0) {
							//printf("- status: %.*s\n", t[i+1].end-t[i+1].start, pmsg + t[i+1].start);
							i++;
						} 
						
						else if (jsoneq(pmsg, &t[i], "pin") == 0) {
							res2 = strndup(pmsg + t[i+1].start, t[i+1].end-t[i+1].start);
							printf("- PIN: %s\n", res2);
							if(strcmp("0",res2) == 0){
								//printf("Sala não está reservada, PIN = 0\n");
								rtc_disable_interrupt(RTC,  RTC_IER_ALREN);
							}
							else{
								strcpy(password_array, res2);
								//Ativa alarme de 5 minutos
								
								if(flag_alarm_enabled == 0){
									printf("Sala reservada. 5 minutos para colocar o PIN\n");
									enableAlarm();
									flag_alarm_enabled = 1;
								}
								
								//printf("PIN recebido: %s", password_array);						
							}
							i++;
						}
						else {
							printf("Unexpected key: %.*s\n", t[i].end-t[i].start, pmsg + t[i].start);
						}
					}
				}
				//https://github.com/zserge/jsmn/blob/master/example/simple.c
				memset(gau8ReceivedBuffer, 0, sizeof(gau8ReceivedBuffer));
				recv(tcp_client_socket, &gau8ReceivedBuffer[0], MAIN_WIFI_M2M_BUFFER_SIZE, 0);
			}
			else{
				close(tcp_client_socket);
				tcp_client_socket = -1;
			}
		}
		break;

		default:
			break;
		}
	}
}

static void set_dev_name_to_mac(uint8_t *name, uint8_t *mac_addr){
	/* Name must be in the format WINC1500_00:00 */
	uint16 len;

	len = m2m_strlen(name);
	if (len >= 5) {
		name[len - 1] = MAIN_HEX2ASCII((mac_addr[5] >> 0) & 0x0f);
		name[len - 2] = MAIN_HEX2ASCII((mac_addr[5] >> 4) & 0x0f);
		name[len - 4] = MAIN_HEX2ASCII((mac_addr[4] >> 0) & 0x0f);
		name[len - 5] = MAIN_HEX2ASCII((mac_addr[4] >> 4) & 0x0f);
	}
}

/**
 * \brief Callback to get the Wi-Fi status update.
 *
 * \param[in] u8MsgType Type of Wi-Fi notification.
 * \param[in] pvMsg A pointer to a buffer containing the notification parameters.
 *
 * \return None.
 */
static void wifi_cb(uint8_t u8MsgType, void *pvMsg){
	switch (u8MsgType) {
		case M2M_WIFI_RESP_CON_STATE_CHANGED:{
			tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
			if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED) {
				//printf("wifi_cb: M2M_WIFI_CONNECTED\r\n");
				m2m_wifi_request_dhcp_client();
			}
			else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED) {
				//printf("wifi_cb: M2M_WIFI_DISCONNECTED\r\n");
				gbConnectedWifi = false;
				wifi_connected = 0;
			}
			break;
		}

		case M2M_WIFI_REQ_DHCP_CONF:{
			uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
			printf("wifi_cb: IP address is %u.%u.%u.%u\r\n",
				pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]);
			wifi_connected = M2M_WIFI_CONNECTED;
		
			/* Obtain the IP Address by network name */
			//gethostbyname((uint8_t *)server_host_name);
			break;
		}

		default:{
			break;
		}
	}
}

void clear_array(char arr[]){
	for(int i = 0; i < 4; i++) {
		arr[i] = 0;
	}
}

void pin_toggle(Pio *pio, uint32_t mask){
	if (pio_get_output_data_status(pio, mask)){
		pio_clear(pio, mask);
	}
	else{
		pio_set(pio, mask);
	}
}

void led_changer(void){
	if(flag_sala_status == 1){
		pio_clear(VERMELHO_PIO, VERMELHO_PIN_MASK); //Liga o LED vermelho
		pio_set(AMARELO_PIO, AMARELO_PIN_MASK); //Desliga LED amarelo de reserva
	}
	if(flag_sala_status == 0){
		pio_set(VERMELHO_PIO, VERMELHO_PIN_MASK); //Desliga o LED vermelho
	}
}

static void button_handler(uint32_t id, uint32_t mask){
	if(flag_sala_status){
		flag_sala_status = 0;
	}
	else{
		flag_sala_status = 1;
	}
	led_changer();
}

void sensor_callback(void){
	flag_sensor = 1;
}

void BUTs_init(void){
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, button_handler);
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 5);
}

void LEDs_init(void){
	pmc_enable_periph_clk(AMARELO_PIO_ID);
	pio_set_output(AMARELO_PIO, AMARELO_PIN_MASK, 0, 0, 0); 
	
	pmc_enable_periph_clk(VERMELHO_PIO_ID);
	pio_set_output(VERMELHO_PIO, VERMELHO_PIN_MASK, 0, 0, 0);
	
	pmc_enable_periph_clk(LEDPLACA_PIO_ID);
	pio_set_output(LEDPLACA_PIO, LEDPLACA_PIN_MASK, 0, 0, 0);
}

void SENSOR_init(void){
	pmc_enable_periph_clk(SENSOR_PIO_ID);
	pio_configure(SENSOR_PIO, PIO_INPUT, SENSOR_PIO_PIN_MASK, PIO_DEFAULT);
	pio_enable_interrupt(SENSOR_PIO, SENSOR_PIO_PIN_MASK);
	pio_handler_set(SENSOR_PIO, SENSOR_PIO_ID, SENSOR_PIO_PIN_MASK, PIO_IT_FALL_EDGE | PIO_IT_RISE_EDGE , sensor_callback);
	NVIC_EnableIRQ(SENSOR_PIO_ID);
	NVIC_SetPriority(SENSOR_PIO_ID, 3);
}

/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void){
	tstrWifiInitParam param;
	int8_t ret;
	uint8_t mac_addr[6];
	uint8_t u8IsMacAddrValid;
	struct sockaddr_in addr_in;

	/* Initialize the board. */
	sysclk_init();
	board_init();
	BUTs_init();
	RTC_init();
	LEDs_init();
	//SENSOR_init();
	led_changer();
	configure_keyboard();
	
	
	flag_sala_status = 0;
	flag_reservada = 0;
	flag_sensor = 0;
	char key = 'x';
	int input_count = 0;
	//clear_array(input);

	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);

	/* Initialize the BSP. */
	nm_bsp_init();
  
	/* Initialize Wi-Fi parameters structure. */
	memset((uint8_t *)&param, 0, sizeof(tstrWifiInitParam));

	/* Initialize Wi-Fi driver with data and status callbacks. */
	param.pfAppWifiCb = wifi_cb;
	ret = m2m_wifi_init(&param);
	if (M2M_SUCCESS != ret) {
		printf("main: m2m_wifi_init call error!(%d)\r\n", ret);
		while (1) {
		}
	}
   	
	/* Initialize socket module. */
	socketInit();
  	
	/* Register socket callback function. */
	registerSocketCallback(socket_cb, resolve_cb);
	
	/* Connect to router. */
	printf("main: connecting to WiFi AP %s...\r\n", (char *)MAIN_WLAN_SSID);
	m2m_wifi_connect((char *)MAIN_WLAN_SSID, sizeof(MAIN_WLAN_SSID), MAIN_WLAN_AUTH, (char *)MAIN_WLAN_PSK, M2M_WIFI_CH_ALL);
	
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = _htons(MAIN_SERVER_PORT);
	inet_aton(MAIN_SERVER_NAME, &addr_in.sin_addr);
	printf("Inet aton : %d", addr_in.sin_addr);

	while(1){
		delay_ms(500);
 		m2m_wifi_handle_events(NULL);
		//getTime();
		// if(flag_sensor){
		// 	if(pio_get(SENSOR_PIO, PIO_INPUT, SENSOR_PIO_PIN_MASK) == 1){
		// 		flag_sala_status = 1;
		// 	}
		// 	else{
		// 		flag_sala_status = 0;
		// 	}
		// 	led_changer();
		// 	flag_sensor = 0;
		// }
		
		key = listen_keyboard();
		if (key != 'x') {
			printf("%c \n",key);
		}

		if(flag_reservada == 1){
			//printf("Count: %d - Input: %s\n", input_count, input);
			//Sala reservada esperando por senha
			if (key != 'x' && key != '#' && key != '*') {
				input[input_count] = key;
				input_count++;
				printf("Senha: %s\n", input);
			}
			if(input_count == 4){
				input_count = 0;
				int value = strcmp(input, password_array);
				if(value == 0){
					printf("Correto. PIN: %s \n", input);
					clear_array(input);
					rtc_disable_interrupt(RTC,  RTC_IER_ALREN);
					printf("Alarme desativado\n");
				}
				else{
					printf("Incorreto. Input: %s - PIN: %s\n", input, password_array);
					printf("Valor de erro: %d \n", value);
					clear_array(input);
				}
			}
		}

		if (wifi_connected == M2M_WIFI_CONNECTED) {  
    		
			if (tcp_client_socket < 0) {
				if ((tcp_client_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
					printf("main: failed to create TCP client socket error!\r\n");
					continue;
				}

				if (connect(tcp_client_socket, (struct sockaddr *)&addr_in, sizeof(struct sockaddr_in)) != SOCK_ERR_NO_ERROR) {
					close(tcp_client_socket);
					tcp_client_socket = -1;
					printf("error\n");
				}
				else{
					gbTcpConnection = true;
				}
			}
		}
	}
	return 0;
}
