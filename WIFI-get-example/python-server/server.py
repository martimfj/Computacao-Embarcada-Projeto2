from flask import Flask, request, render_template, redirect
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

todos = {}

g_nome = "vazia"
g_reserva = "nao"

class HelloWorld(Resource):
    def get(self, nome):
        global g_nome
        global g_reserva

        g_nome = nome
        if g_nome == "cheia":
            g_reserva = "nao"

        return {'status': g_nome,
                'reserva': g_reserva}

class ReservaClass(Resource):
    def get(self):
        global g_reserva

        return g_reserva


#class TodoSimple(Resource):
#    def get(self, todo_id):
#        return {todo_id: todos[todo_id]}
#
#    def put(self, todo_id):
#        todos[todo_id] = request.form['data']
#        return {todo_id: todos[todo_id]}

api.add_resource(ReservaClass, '/api/reserva/')
api.add_resource(HelloWorld, '/api/status/<string:nome>')

@app.route("/")
def main_page():
        return render_template("index.html", name=g_nome, reserva=g_reserva)

@app.route("/reserva/<string:reserva>")
def reserva_page(reserva):
    global g_reserva
    g_reserva = reserva
    return redirect("/")

@app.route("/status/<string:nome>")
def status_page(nome):
    global g_nome
    g_nome = nome
    return redirect("/")

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)  
    #app.run(debug=True)

